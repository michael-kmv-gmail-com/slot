<?php
/**
 * Use "php artisan slot" to run this command
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;

class SlotCommand extends Command
{
    const COLS = 5;
    const ROWS = 3;
    const SYMBOLS = ['9', '10', 'J', 'Q', 'K', 'A', 'cat', 'dog', 'monkey', 'bird'];
    const BET = 1;
    const RULES = [5=>1000, 4=>200, 2=>10];

    private $board = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run slot command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Generate board
     */
    private function generateBoard()
    {
        for ($i=0; $i<self::ROWS; $i++) {
            for ($j=0; $j<self::COLS; $j++) {
                $this->board[$i][$j] = self::SYMBOLS[rand(0, 9)];
            }
        }
    }

    /**
     * Get matches for exact row
     *
     * @param array $row
     * @return array
     */
    private function getSymbolsAmount($row)
    {
        $symbols = [];
        $currentSymbol = $row[0];
        $currentAmount = 1;
        for ($i=1; $i<count($row); $i++) {
            if ($currentSymbol == $row[$i]) $currentAmount++;
            else {
                $symbols[] = [$currentSymbol => $currentAmount];
                $currentSymbol = $row[$i];
                $currentAmount = 1;
            }
        }
        $symbols[] = [$currentSymbol => $currentAmount];

        return $symbols;
    }

    /**
     * Get matches amount for one row
     *
     * @param array $rows
     * @return array
     */
    private function calculateMatchesAmount($row)
    {
        $result = [];
        foreach ($row as $data) {
            $v = $data[key($data)];
            foreach (self::RULES as $cnt => $per) {
                if ($v == $cnt) $result[] = $cnt;
            }
        }

        return $result;
    }

    /**
     * Get board matches: [row => [matches]]

     * @return array
     */
    private function getBoardMatches()
    {
        // Get matches liat
        for ($i=0; $i<self::ROWS; $i++) {
            $rows[] = $this->getSymbolsAmount($this->board[$i]);
        }

        // Calculate matches amount
        $matches = [];
        for ($i=0; $i<count($rows); $i++) {
            $matches[] = $this->calculateMatchesAmount($rows[$i]);
        }

        return $matches;
    }

    /**
     * Create array with column - row numeration
     *
     * @param int $rowNum
     * @return array
     */
    private function getColumnRowArray($rowNum)
    {
        $sum = $rowNum;
        $result = [$sum];
        for ($i=1; $i<self::COLS; $i++) {
            $sum += self::ROWS;
            $result[] = $sum;
        }

        return $result;
    }

    private function printResult($matches)
    {
        $bet = self::BET * 100;

        // Board
        $lines = [];
        for ($i=0; $i<self::ROWS; $i++) {
            $lines[] = implode(',', $this->board[$i]);
        }
        echo '['.implode(',', $lines)."]\n";

        // Paylines
        $paylines = [];
        for ($i=0; $i<count($matches); $i++) {
            if (count($matches[$i]) == 0) continue;
            $paylines[] = '{'.implode(',', $this->getColumnRowArray($i)).':'.implode(':', $matches[$i]).'}';
        }
        if (count($paylines) > 0) echo '['.implode(',', $paylines).']'."\n";

        // Bet amount
        echo "bet_amount: {$bet}\n";

        // Total win
        $totalWin = 0;
        for ($i=0; $i<count($matches); $i++) {
            if (count($matches[$i]) == 0) continue;
            foreach ($matches[$i] as $index => $match) {
                foreach (self::RULES as $cnt => $per) {
                    if ($match == $cnt) $totalWin += $bet * $per / 100;
                }
            }
        }

        echo "total_win: {$totalWin}\n";
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generateBoard();
        $matches = $this->getBoardMatches();
        $this->printResult($matches);
    }
}
